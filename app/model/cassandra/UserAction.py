from cassandra.cqlengine import columns
from app.model.cassandra.Base import Base


class UserAction(Base):
    action = columns.Text(primary_key=True)
    user_id = columns.Integer(partition_key=True)
    related = columns.Text()
    added_on = columns.DateTime(primary_key=True)

    def get_data(self):
        return {
            'user_id': self.user_id,
            'action': self.action,
            'added_on': self.added_on,
            'related': self.related
        }
