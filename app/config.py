import os

from cassandra.auth import PlainTextAuthProvider
from dotenv import load_dotenv

# load env
APP_ROOT = os.path.join(os.path.dirname(__file__), '..')
dotenv_path = os.path.join(APP_ROOT, '.env')
load_dotenv(dotenv_path)

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    # TODO sensitive connectionString and token should be stored on an environment file, removed to avoid complexity
    SECRET_KEY = 'ca64eb3c0175bd14c5d500fce2d443553110539d2b59'
    JWT_SECRET_KEY = 'e5ccca8cc12f2710cb982c3b6f8803397901aaadfd45'
    SQLALCHEMY_DATABASE_URI = 'postgres://postgres:nicePassword@arvm5.ar:5432/wapp'
    CASSANDRA_HOST = ['192.168.33.11']
    CASSANDRA_AUTH = PlainTextAuthProvider(username='cassandra', password='cassandra')
    CASSANDRA_KEYSPACE = "wproject_actions"


class ProductionConfig(Config):
    DEBUG = False


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(Config):
    TESTING = True