from flask import Blueprint, jsonify, request

from app.helpers.EventLog import EventLog
from flask_jwt_extended import (jwt_required, get_jwt_identity)

from app.helpers.QueryHelper import QueryHelper
from app.model.cassandra.UserAction import UserAction

routes_api = Blueprint('routes_api', __name__)


@routes_api.route('/eventRoute')
@jwt_required
def event_route():
    identity = get_jwt_identity()

    if identity is None:
        response = {
            'message': 'this_endpoint_need_a_valid_session_token'
        }
        return jsonify(response), 400

    # log call event cassandra
    # TODO handle exception
    EventLog.send_event('apiCall', 'logApiEventRoute', identity)

    response = {
        'message': 'thank_you_for_visiting_your_event_is_logged'
    }
    return jsonify(response), 200


@routes_api.route('/getEventRoute')
@jwt_required
def get_event_route():
    identity = get_jwt_identity()

    if get_jwt_identity() is None:
        response = {
            'message': 'this_endpoint_need_a_valid_session_token'
        }
        return jsonify(response), 400

    # get action from param action
    action_arg = request.args.get('action', default='pageView', type=str)

    # TODO make distinct from query
    stored_actions = UserAction.objects.allow_filtering().filter(action=action_arg).all()

    # distinct data
    actions = QueryHelper.distinct_user(stored_actions)

    # log call event cassandra
    # TODO handle exception
    EventLog.send_event('apiCall', 'logApiGetEventRoute', identity)

    return jsonify(actions)
