import os

from cassandra.cqlengine import connection
from flask_jwt_extended import JWTManager
from cassandra.cluster import Cluster
from flask import Flask
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api

from app import config
from app.model.cassandra import UserAction

basedir = os.path.abspath(os.path.dirname(__file__))
db = SQLAlchemy()


def create_app():
    # init app
    app = Flask(__name__, template_folder='template')
    api = Api(app)

    # enable debug
    app.debug = True

    # set configuration mode to development
    app.config.from_object(config.DevelopmentConfig)
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['JWT_BLACKLIST_ENABLED'] = False
    app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']

    # init db
    db.init_app(app)
    # needed for pgsql migration script
    migrate = Migrate(app, db)

    @app.before_first_request
    def create_tables():
        db.create_all()

    login_manager = LoginManager()
    login_manager.login_view = 'pages.login_page'
    login_manager.init_app(app)

    from .model.pgsql import User

    # flask_login recommended to load current user
    @login_manager.user_loader
    def load_user(user_id):
        return User.User.query.get(int(user_id))

    # register blueprints
    from .auth_ui import auth_ui as auth_ui_blueprint
    app.register_blueprint(auth_ui_blueprint)

    from app.api.auth_api import auth_api as auth_api_blueprint
    app.register_blueprint(auth_api_blueprint)

    from app.api.routes_api import routes_api as routes_api_blueprint
    app.register_blueprint(routes_api_blueprint)

    from .pages_ui import pages_ui as pages_blueprint
    app.register_blueprint(pages_blueprint)

    # cassandra init connection and KEYSPACE
    cluster = Cluster(config.Config.CASSANDRA_HOST, protocol_version=3, auth_provider=config.Config.CASSANDRA_AUTH)
    session = cluster.connect()
    session.execute(
        """
        CREATE KEYSPACE IF NOT EXISTS %s WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 };
        """ % config.Config.CASSANDRA_KEYSPACE)

    session = cluster.connect(keyspace=config.Config.CASSANDRA_KEYSPACE)

    # add jwt support on app
    app.config['JWT_SECRET_KEY'] = 'jwt-secret-string'
    app.config['JWT_BLACKLIST_ENABLED'] = False
    jwt = JWTManager(app)

    cass_connection = connection.setup(config.Config.CASSANDRA_HOST, protocol_version=3,
                                       auth_provider=config.Config.CASSANDRA_AUTH,
                                       default_keyspace="cqlengine")
    return app
