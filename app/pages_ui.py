from flask import Blueprint, render_template, jsonify, request
from flask_login import login_required, current_user

from app.helpers.EventLog import EventLog
from app.helpers.QueryHelper import QueryHelper
from app.model.cassandra.UserAction import UserAction

pages_ui = Blueprint('pages', __name__)


@pages_ui.route('/')
@login_required
def main_page():

    # log visit event cassandra
    # TODO handle exception
    EventLog.send_event('pageView', 'logEventPage', current_user.id)

    return render_template('main.html', name=current_user.name)


@pages_ui.route('/getEvents')
@login_required
def events_page():
    # get action from param action
    action_arg = request.args.get('action', default='pageView', type=str)

    # TODO make distinct from query
    # stored_actions = UserAction.objects.filter(action=action_arg).all()
    stored_actions = UserAction.objects.allow_filtering().filter(action=action_arg).all()

    # distinct data
    actions = QueryHelper.distinct_user(stored_actions)

    # log visit event cassandra
    # TODO handle exception
    EventLog.send_event('pageView', 'getEventPage', current_user.id)

    return jsonify(actions)


@pages_ui.route('/login')
def login_page():
    return render_template('login.html')


@pages_ui.route('/register')
def register_page():
    return render_template('register.html')
