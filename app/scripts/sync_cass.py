from cassandra.cqlengine import connection
from cassandra.cqlengine.management import sync_table
from app.model.cassandra.UserAction import UserAction
from app import config

connection.setup(config.Config.CASSANDRA_HOST, protocol_version=3, auth_provider=config.Config.CASSANDRA_AUTH,
                 default_keyspace="cqlengine")

sync_table(UserAction)
