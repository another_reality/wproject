class QueryHelper:

    @staticmethod
    def distinct_user(stored_actions):
        dict = set()
        actions = list()
        for action in stored_actions:
            if not actions:
                actions.append(action.get_data())
                dict.add(action.user_id)
            else:
                if action.user_id not in dict:
                    actions.append(action.get_data())
                    dict.add(action.user_id)

        return actions
