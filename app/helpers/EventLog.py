from datetime import datetime
from app.model.cassandra.UserAction import UserAction


class EventLog:

    @staticmethod
    def send_event(action, related, user_id):
        log_action = UserAction.create(action=action, added_on=datetime.now(), user_id=user_id,
                                       related=related)
        log_action.save()
