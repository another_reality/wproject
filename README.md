# WProject

WProject is a Python Flask App with login - register and log event functionality.

## Installation

Create Python virtual environment

```bash
python3.7 -m venv venv
```

Install project requirement

```bash
venv/bin/pip3.7 install -r requirements.txt
```

Setup project with setuptools

```bash
venv/bin/python3.7 setup.py install
```

## Database migration

Update configuration file

```bash
nano app/config.py
```

Postgres migration

```bash
venv/bin/python3.7 app/scripts/migrate_pgsql.py db init
venv/bin/python3.7 app/scripts/migrate_pgsql.py db migrate
venv/bin/python3.7 app/scripts/migrate_pgsql.py db upgrade
```

Cassandra migration

```bash
venv/bin/python3.7 app/scripts/sync_cass.py
```

## Environment

```bash
PYTHONPATH=/wproject/venv/lib/python3.7/site-packages
FLASK_APP=app/__init__.py
```

## TODO

 - Tests
 - Api Documentation (Swagger)
 - Sensitive connectionString and token should be stored on an environment file, removed to avoid complexity
 - Make distinct from Cassandra query
 - Handle error and exceptions

## License
[MIT](https://choosealicense.com/licenses/mit/)